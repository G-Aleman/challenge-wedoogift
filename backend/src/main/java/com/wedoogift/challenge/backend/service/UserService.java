package com.wedoogift.challenge.backend.service;

import com.wedoogift.challenge.backend.entity.Distribution;
import com.wedoogift.challenge.backend.entity.Gift;
import com.wedoogift.challenge.backend.entity.User;
import com.wedoogift.challenge.backend.repository.DistributionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private DistributionRepository distributionRepository;

    public Float getUserDistributionBalance(User user){
        List<Distribution> distributionList = distributionRepository.findAllByUser(user);
        Float userGiftBalance = 0f;
        for (Distribution distribution: distributionList) {
            if (Calendar.getInstance().getTime().before(distribution.getExpirationDate())) {
                userGiftBalance += distribution.getAmount();
            }
        }
        return userGiftBalance;
    }
}
