package com.wedoogift.challenge.backend.utils;

public class DateUtils {

    public static boolean isBisextile(int year) {
        if (year % 400 == 0 || year % 4 ==0 && year % 100 != 0) {
            return true;
        }
        return false;
    }
}
