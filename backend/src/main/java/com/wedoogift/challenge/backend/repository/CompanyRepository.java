package com.wedoogift.challenge.backend.repository;

import com.wedoogift.challenge.backend.entity.Company;
import org.springframework.data.repository.CrudRepository;

public interface CompanyRepository extends CrudRepository<Company, Integer> {
}
