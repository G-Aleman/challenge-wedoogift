package com.wedoogift.challenge.backend.repository;

import com.wedoogift.challenge.backend.entity.Distribution;
import com.wedoogift.challenge.backend.entity.Gift;
import com.wedoogift.challenge.backend.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DistributionRepository extends CrudRepository<Distribution, Integer> {

    List<Distribution> findAllByUser(User user);
}
