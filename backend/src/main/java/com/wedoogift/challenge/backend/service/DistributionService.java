package com.wedoogift.challenge.backend.service;

import com.wedoogift.challenge.backend.entity.*;
import com.wedoogift.challenge.backend.repository.CompanyRepository;
import com.wedoogift.challenge.backend.repository.DistributionRepository;
import com.wedoogift.challenge.backend.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Calendar;
import java.util.Date;

@Service
public class DistributionService {

    @Autowired
    private DistributionRepository distributionRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Transactional
    public Distribution giftDeposit(Company company, User user, float amount) throws Exception {
        Distribution gift = createDistribution(company, user,amount);

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, 1);
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        Date expirationDate = calendar.getTime();
        gift.setExpirationDate(expirationDate);

        company.setBalance(company.getBalance()-amount);
        companyRepository.save(company);
        return distributionRepository.save(gift);

    }

    @Transactional
    public Distribution mealDeposit(Company company, User user, float amount) throws Exception {
        Distribution meal = createDistribution(company, user,amount);


        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR)+1, 2, DateUtils.isBisextile(calendar.YEAR)?29:28);
        Date expirationDate = calendar.getTime();
        meal.setExpirationDate(expirationDate);

        company.setBalance(company.getBalance()-amount);
        companyRepository.save(company);
        return distributionRepository.save(meal);

    }

    private Distribution createDistribution(Company company, User user, float amount) throws Exception {
        if(amount < 0) {
            throw new Exception("It is not possible to make a negative gift");
        } else if (company.getBalance() < amount) {
            throw new Exception("The company does not have enough money");
        }
        Gift gift = new Gift();
        gift.setCompany(company);
        gift.setUser(user);
        gift.setAmount(amount);
        Calendar calendar = Calendar.getInstance();
        Date todayDate = calendar.getTime();
        gift.setReceivedDate(todayDate);
        return gift;

    }

}
