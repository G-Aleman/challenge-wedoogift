package com.wedoogift.challenge.backend.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DateUtilsTest {

    @Test
    void shouldTestDate()  {
        Assertions.assertTrue(!DateUtils.isBisextile(1900));
        Assertions.assertTrue(DateUtils.isBisextile(2020));
        Assertions.assertTrue(!DateUtils.isBisextile(2021));
        Assertions.assertTrue(DateUtils.isBisextile(2000));
    }

}