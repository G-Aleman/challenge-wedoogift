package com.wedoogift.challenge.backend.service;

import com.wedoogift.challenge.backend.entity.*;
import com.wedoogift.challenge.backend.repository.CompanyRepository;
import com.wedoogift.challenge.backend.repository.DistributionRepository;
import com.wedoogift.challenge.backend.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DistributionServiceTest {

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DistributionRepository distributionRepository;

    @Autowired
    private DistributionService distributionService;

    @Test
    void shouldSave100Gift() throws Exception {
        Company company = companyRepository.findById(1).orElseThrow(() -> new Exception("Cannot get Company with Id 1 from DB"));
        User user = userRepository.findById(1).orElseThrow(() -> new Exception("Cannot get User with Id 1 from DB"));
        Distribution gift = distributionService.giftDeposit(company, user, 1);
        Gift giftInDB = (Gift) distributionRepository.findById(gift.getId()).orElseThrow(() -> new Exception("Cannot get Gift with Id "+ gift.getId()+" from DB"));;
        Assertions.assertEquals(1, gift.getAmount());
        Assertions.assertEquals(1, giftInDB.getAmount());
    }

    @Test
    void shouldNotSaveNegativeGift() throws Exception {
        Company company = companyRepository.findById(1).orElseThrow(() -> new Exception("Cannot get Company with Id 1 from DB"));
        User user = userRepository.findById(1).orElseThrow(() -> new Exception("Cannot get User with Id 1 from DB"));
        Assertions.assertThrowsExactly(Exception.class, () -> {
            distributionService.giftDeposit(company, user, -100);});

    }

    @Test
    void shouldNotSaveNegativeBalanceForCompany() throws Exception {
        Company company = companyRepository.findById(1).orElseThrow(() -> new Exception("Cannot get Company with Id 1 from DB"));
        User user = userRepository.findById(1).orElseThrow(() -> new Exception("Cannot get User with Id 1 from DB"));
        Assertions.assertThrowsExactly(Exception.class, () -> {
            distributionService.giftDeposit(company, user, 100000);});

    }

    @Test
    void shouldSave50Meal() throws Exception {
        Company company = companyRepository.findById(1).orElseThrow(() -> new Exception("Cannot get Company with Id 1 from DB"));
        User user = userRepository.findById(1).orElseThrow(() -> new Exception("Cannot get User with Id 1 from DB"));
        Distribution meal = distributionService.mealDeposit(company, user, 50);
        Distribution mealInDB = distributionRepository.findById(1).orElseThrow(() -> new Exception("Cannot get Meal with Id 1 from DB"));;
        Assertions.assertEquals(50, meal.getAmount());
        Assertions.assertEquals(50, mealInDB.getAmount());
    }
}