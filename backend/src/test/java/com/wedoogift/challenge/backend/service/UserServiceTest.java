package com.wedoogift.challenge.backend.service;

import com.wedoogift.challenge.backend.entity.Company;
import com.wedoogift.challenge.backend.entity.Distribution;
import com.wedoogift.challenge.backend.entity.User;
import com.wedoogift.challenge.backend.repository.CompanyRepository;
import com.wedoogift.challenge.backend.repository.DistributionRepository;
import com.wedoogift.challenge.backend.repository.UserRepository;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Calendar;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class UserServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private DistributionRepository distributionRepository;

    @Autowired
    private DistributionService distributionService;

    @Test
    @Order(1)
    void shouldGetUserBalanceAndReturn0() throws Exception {
        User user = userRepository.findById(1).orElseThrow(() -> new Exception("An error Occured"));
        Assertions.assertEquals(0, userService.getUserDistributionBalance(user));
    }

    @Test
    @Order(2)
    void shouldTestTheExpiryOfGift() throws Exception {
        User user = userRepository.findById(1).orElseThrow(() -> new Exception("An error Occured"));
        Assertions.assertEquals(0, userService.getUserDistributionBalance(user));
        Company company = companyRepository.findById(1).orElseThrow(() -> new Exception("Cannot get Company with Id 1 from DB"));
        Distribution gift = distributionService.giftDeposit(company, user, 100);
        Assertions.assertEquals(100, userService.getUserDistributionBalance(user));
        // change the date of the Gift
        Calendar calendar= Calendar.getInstance();
        calendar.add(Calendar.YEAR, -1);
        gift.setExpirationDate(calendar.getTime());
        distributionRepository.save(gift);
        Assertions.assertEquals(0, userService.getUserDistributionBalance(user));

    }

    @Test
    @Order(3)
    void shouldTestTheExpiryOfMeal() throws Exception {
        User user = userRepository.findById(1).orElseThrow(() -> new Exception("An error Occured"));
        Assertions.assertEquals(0, userService.getUserDistributionBalance(user));
        Company company = companyRepository.findById(1).orElseThrow(() -> new Exception("Cannot get Company with Id 1 from DB"));
        company.setBalance(200f);
        companyRepository.save(company);
        Distribution meal = distributionService.mealDeposit(company, user, 100);
        Assertions.assertEquals(100, userService.getUserDistributionBalance(user));
        // change the date of the Gift
        Calendar calendar= Calendar.getInstance();
        calendar.add(Calendar.YEAR, -1);
        meal.setExpirationDate(calendar.getTime());
        distributionRepository.save(meal);
        Assertions.assertEquals(0, userService.getUserDistributionBalance(user));

    }

    @Test
    @Order(4)
    void shouldGetUserBalanceAndReturn100() throws Exception {
        User user = userRepository.findById(1).orElseThrow(() -> new Exception("An error Occured"));
        Assertions.assertEquals(0, userService.getUserDistributionBalance(user));
        Company company = companyRepository.findById(1).orElseThrow(() -> new Exception("Cannot get Company with Id 1 from DB"));
        distributionService.giftDeposit(company, user, 50);
        Assertions.assertEquals(50, userService.getUserDistributionBalance(user));
        distributionService.mealDeposit(company, user, 50);
        Assertions.assertEquals(100, userService.getUserDistributionBalance(user));

    }
}